FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG NODE_VERSION=22.11.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
      curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | \
      tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=Requarks/wiki versioning=semver extractVersion=^v(?<version>.+)$
ARG WIKIJS_VERSION=2.5.306

# chown is required because wikijs wants to read package.json
RUN curl -L https://github.com/Requarks/wiki/releases/download/v${WIKIJS_VERSION}/wiki-js.tar.gz | tar zxf - -C /app/code/ && \
    rm -f /app/code/config.sample.yml && ln -sf /app/data/config.yml /app/code/config.yml && \
    ln -s /app/data/ssh /home/cloudron/.ssh && \
    chown -R cloudron:cloudron /app/code

COPY config.yml.template start.sh /app/pkg/

ENV NODE_ENV production

CMD [ "/app/pkg/start.sh" ]
