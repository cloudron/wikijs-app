### Overview

Wiki.js is a powerful and extensible open source Wiki software.
It aims to make writing documentation a joy through a beautiful and intuitive interface.

### Features
 * Performance: Running on the blazing fast Node.js engine, Wiki.js is built with performance in mind.
 * Protected: Make your wiki public, completely private or a mix of both.
 * Administration: Manage all aspects of your wiki using the extensive and intuitive admin area.
 * Customizable: Fully customize the appearance of your wiki, including a light and dark mode.
 * Extensible: There is a wide range of [modules](https://wiki.js.org/modules) available
