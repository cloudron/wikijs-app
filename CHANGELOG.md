[1.0.0]
* Initial release

[1.1.0]
* Update to 2.1.113

[1.2.0]
* Update to 2.2.51

[1.3.0]
* Use latest base image 2.0.0

[1.3.1]
* Update to 2.3.77

[1.3.2]
* Update to 2.3.81

[1.4.0]
* Update to 2.4.75
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.4.75)
* fix: setup assets location + mysql migration 2.4.13
* fix: hide comment count (wip)
* feat: edit comment
* feat: comments post min delay
* feat: comments delete + refresh on post + formatting
* fix: admin security UI

[1.5.0]
* Update WikiJS to 2.4.105
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.4.105)
* fix: strip starting slash from path during page create
* fix: dashboard invalid version on load
* fix: site config host slice
* fix: respect image floating from visual editor (#1981)
* fix: roboto-mono incorrect path for arabic locales
* fix: update saved state for page meta
* fix: stop search loading when exiting

[1.5.1]
* Update WikiJS to 2.4.107
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.4.107)
* fix: use quote icon for plain blockquote
* fix: secure html module removes target attribute from links (#2012)

[1.5.2]
* Fix up screenshots and add forum url in manifest

[1.5.3]
* Update WikiJS to 2.5.126
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.126)
* fix: remove unused dep in nav-header
* fix: search autocomplete off
* fix: elastic apm rum client script
* fix: 2.5.108 migration
* fix: migration error for new installs
* feat: purge history utility
* feat: handle disabled auth strategies
* fix: login screen UI on dark mode
* fix: discord auth module new URL. (#2390)
* feat: ldap avatar support

[1.5.4]
* Update Wiki.js to 2.5.132
* fix: prevent write:groups from self-promoting
* fix: logout URL endpoint option for oauth2 module
* fix: 2fa qr code - handle special chars in site title
* fix: force lowercase for email on local auth
* fix: bypass page rule check for global permission check + handle missing page extra field

[1.5.5]
* Update Wiki.js to 2.5.136
* fix: API key incorrectly forces token revalidation
* fix: draw.io svgs are no longer removed with linebreaks (#2415)
* fix: checkExclusiveAccess incorrectly includes root admin

[1.6.0]
* Update Wiki.js to 2.5.144
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.144)
* fix: missing localization strings for page select dialog
* fix: missing localization strings in login screen
* fix: login bg insert from asset not working
* fix: some auth icons are monochrome
* fix: security html module removes allow attribute from iframes (#2354)
* fix: support permissions by tags for basic db search engine (#2416)
* fix: hidden download button in admin locale page on smaller screens (#2429)

[1.7.0]
* Update Wiki.js to 2.5.159
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.159)
* fix: remove bugsnag + update deps
* fix: bypass auth redirect cookie when set to homepage
* fix: handle missing extra field during page render
* fix: set enableArithAbort explicit value for tedious driver
* fix: check for email array during processProfile (#2515)
* fix: update Matomo integration client code (#2526)
* fix: strip directory traversal sequences from asset paths

[1.7.1]
* Update Wiki.js to 2.5.170
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.170)
* fix: revert refactor in markdown-kroki and plantuml modules (#2619)
* fix: invalid graphql dep version (2) (#2617)
* fix: invalid graphql dep version (#2616)
* Merge pull request from GHSA-pgjv-84m7-62q7
* refactor: server code (#2545)
* fix: incorrect cancel button i18n key (#2543)

[1.8.0]
* Update base image to v3

[1.8.1]
* Update Wiki.js to 2.5.191
* add v-pre to pre tags at render time
* syntax error in rebuild-tree.js (#3048)
* get syncInterval from model instead of module data (#3003)
* LDAP - avoid reading empty tls cert file (#2980)
* in group edit rules, write scripts permission and write styles permission can be configured. (#2829)
* rebuilding tree error when the page number is large enough in sqlite (#2830)
* update storage.js to match pageHelper.injectPageMetadata (#2832)
* ability to open search result in new tab with middle-click (#2919)
* set autocommit for mysql (#2638)
* support tags containing special characters (#2743) (#2748)
* search engine broken when renaming or moving pages (#2815)
* set analyzer for elasticsearch (#2793)
* use absolute URL for logo in email if path relative (#2628)
* broken draw io diagram on rtl mode, improve elasticsearch config (#2647)
* enable passport-azure-ad workaround for SameSite cookies (#2567)
* superscript typo in module definition.yml (#2577)
* media print css (#2593)
* inline math interpreted as attributes (#2645) [ #1581 ]

[1.8.2]
* Update Wiki.js to 2.5.197
* handle raw mustache expressions over multiple lines
* disable cors
* S3 copyObject usage - Missing bucket name (#3745)
* loginRedirect doesn't work for non local strategies (#3222)
* HSTS header max-age value (#3225)

[1.8.3]
* Update Wiki.js to 2.5.201
* fix: convert page - handle tabsets
* fix: convert page - task list + UI fixes
* feat: convert page

[1.8.4]
* Update Wiki.js to 2.5.214
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.214)
* fix: anchor links - use MouseEvent.currentTarget (#4236)
* fix: remove unused middleware
* fix: anchor links - use MouseEvent.currentTarget (#4236)
* fix: add missing decodeURIComponent while page load (#4244)
* fix: resolve admin pages pagination bug (#4280)
* fix: git storage - handle renamed files & assets (#4307)
* fix: replace passport-slack implementation (#4369)
* docs: fix helm ingress doc
* feat(helm): adding liveness and readiness probes customizations (#4116)
* feat: add support of hd auth parameter to work with G Suite domains (#4010)
* fix: disallow # char in file uploads (#3770)

[1.8.5]
* Update Wiki.js to 2.5.219
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.219)

[1.8.6]
* Update Wiki.js to 2.5.254
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.254)
* Use NodeJs 16.13.1

[1.8.7]
* Update Wiki.js to 2.5.255
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.255)

[1.8.8]
* Update Wiki.js to 2.5.260
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.260)
* Update base image to 3.2.0

[1.9.0]
* Various fixes for git storage backend to work

[1.9.1]
* Update Wiki.js to 2.4.264
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.264)

[1.9.2]
* Update Wiki.js to 2.4.268
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.268)
* fix: add scrollbar for big diagrams (#4120)
* fix: scanSVG incorrect ext reference (#4825)
* docs: fix typo on helm documentation (#4791)

[1.9.3]
* Update Wiki.js to 2.4.272
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.272)
* misc: update dependencies
* fix: force uploads to use auth headers instead of cookie
* fix: view source of page version crash (#3297)

[1.9.4]
* Update Wiki.js to 2.4.274
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.274)
* fix: prevent upload bypass via uppercase path

[1.9.5]
* Update Wiki.js to 2.5.276
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.276)

[1.9.6]
* Update Wiki.js to 2.5.277
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.277)

[1.9.7]
* Update Wiki.js to 2.5.279
* [Full changelog](https://github.com/Requarks/wiki/releases/tag/2.5.279)

[1.9.8]
* Update Wiki.js to 2.5.280
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.280)
* 559b8ae - undefined author in git commit when deleting a page (PR #5215 by @myml)

[1.9.9]
* Update Wiki.js to 2.5.281
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.281)
* 78d02dc - prevent manage system assignment from manage groups permission (commit by @NGPixel)
* b3731dd - handle unicode chars when syncing files using git storage (PR #5272 by @myml)
* d9076c4 - typo in saml auth module (commit by @NGPixel)

[1.9.10]
* Update Wiki.js to 2.5.282
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.282)

[1.9.11]
* Update Wiki.js to 2.5.283
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.283)
* 89cbb19 - site tree not working because of incorrect variable name (PR #5304 by @myml)

[1.10.0]
* Update Wiki.js to 2.5.284
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.284)
* 97a7445 - mail: allow setting of mailer identifying name (PR #5363 by @davwheat)
* e3d94f7 - don't push files to git if ignored (PR #5334 by @Fireant456)
* b78026e - auth: handle null SAML authnContext context (commit by @NGPixel)
* 046e4b9 - graphql: remove required flag on MailConfig schema (commit by @NGPixel)
* 1e57773 - mail: typo in admin mail save mutation (commit by @NGPixel)
* 18f9165 - graphql: add missing admin mail name variable to save mutation (commit by @NGPixel)

[1.10.1]
* Update Wiki.js to 2.5.285
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.285)
* admin: make page extensions configurable

[1.10.2]
* Update Wiki.js to 2.5.286
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.286)
* 4f2dd36 - helm: expose DATABASE_URL (PR #5445 by @dnplkndll)
* 628c72e - CAS authentication module (PR #5452 by @SeaLife)
* dffffd3 - helm: allow self-signed ssl (PR #5446 by @dnplkndll)
* e78953d - admin: update admin groups page rules write:pages label to match actual permissions (commit by @NGPixel)

[1.10.3]
* Update Wiki.js to 2.5.287
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.287)
* 4e5da41 - page cursor and pagination (PR #5541 by @sandhya-veludandi)
* 31bd327 - correct azure blob storage typo (PR #5591 by @jaredbrogan)

[1.10.4]
* Update Wiki.js to 2.5.288
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.288)
* 4b30050 - prevent user enumeration using local login timings (commit by @NGPixel, reported by @Vautia on huntr.dev)
* ebf4da9 - oidc auth groups relate / unrelate (commit by @NGPixel)

[1.10.5]
* Update Wiki.js to 2.5.289
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.289)
* 8715cd6 - edit shortcuts (commit by @NGPixel)
* fa35f3a - footer text overflow on smaller desktop screens (commit by @NGPixel)

[1.10.6]
* Update Wiki.js to 2.5.290
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.290)
* `America/Sao_Paulo` timezone offset (PR #5690 by @ecarruda)
* comment edit not updating original content (PR #5646 by @adroslice)
* typo in letsencrypt.js logging output (PR #5712 by @cleaverm)
* add missing scriptJs and scriptCss to single page resolver (PR #5689 by @rrg92)
* oidc module - map() call on undefined; fix unrelate() usage (PR #5781 by @asenchuk)
* admin contribute link address. (PR #5791 by @JiuLing-zhang)

[1.10.7]
* Do not overwrite autoEnrollGroups and domainWhitelist

[1.10.8]
* Fix quoting when email display name has single quote

[1.10.9]
* Update Wiki.js to 2.5.291
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.291)
* 9fbc25a - improve table rendering + add markdown-it-decorate module (commit by @NGPixel)
* 1893fd4 - login with Keycloak 20 by explicit set OAuth scopes (PR #5808 by @silicht)
* 2cb3041 - Page Rules based on Tag Matches do not work for comment permissions (PR #5819 by @natsutteatsuiyone)
* 445ad05 - incompatibility issues with passport-openidconnect@0.1.1 (PR #5799 by @natsutteatsuiyone)

[1.10.10]
* Update Wiki.js to 2.5.292
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.292)
* db2ad81 - katex persistent macro support (PR #5838 by @cannorin)
* d10f2a1 - send `UPGRADE_COMPANION_REF` in automated upgrade call (commit by @NGPixel)
* 86c9407 - add umami analytics module (PR #5869 by @CDN18)
* 0a2a32d - add artalk comment module (PR #5868 by @CDN18)

[1.10.11]
* Update Wiki.js to 2.5.294
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.294)
* f3133a7 - toc sidebar position (commit by @NGPixel)
* 73af37b - git log should explicitly separate branch from paths (PR #5911 by @CDN18)
* c91ff2d - add page-ready client boot event (commit by @NGPixel)
* eb99f82 - edit buttons hidden when toc sidebar is on the right (commit by @NGPixel)
* d6d88ed - edit buttons header alignment when toc sidebar is on the right (commit by @NGPixel)

[1.10.12]
* Update Wiki.js to 2.5.295
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.295)
* 54dbf9a - add asciidoc editor module (PR #5954 by @dzruyk)
* eadefb8 - sideloading locales should import availabilities (PR #5973 by @topdev-spetermann)

[1.10.13]
* Update Wiki.js to 2.5.296
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.296)
* 12233c4 - enable state key on generic oauth2 (PR #6104 by @Sleuth56)
* 5f876ce - optional oauth2 module nonce toggle (commit by @NGPixel)
* 43a797d - adds displayName property to OIDC authentication module (PR #6096 by @gueldenstone)
* 1da80ea - oauth2 add groups mapping (PR #6053 by @utix)
* 0d914b0 - add singleByPath GraphQL resolver (PR #6011 by @leangseu)
* 8fa771c - set groups based on LDAP groups (PR #5903 by @icsinfo)
* 41454cf - git: disable color.ui in git storage (PR #6014 by @EricFromCanada)
* 2e85854 - git: handle file renames between folders (PR #6020 by @EricFromCanada)
* ac930fc - change simple-git import (commit by @NGPixel)

[1.10.14]
* Update Wiki.js to 2.5.297
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.297)
* 78a35c3 - include query parameters in locale redirect (PR #6132 by @dbnicholson)
* e954b50 - footer text override option (commit by @NGPixel)
* bba1d1b - oidc: use `_json` prop when setting displayName (PR #6135 by @gueldenstone)
* 26dcc00 - sort visualize tree (PR #6110 by @leangseu)
* f972a9c - code block styling in blockquotes (PR #6108 by @Nyxtorm)
* e495e0a - update google analytics field help to refer to the new ID format (commit by @NGPixel)
* 490fab1 - handle empty ToC position value (commit by @NGPixel)

[1.10.15]
* Update Wiki.js to 2.5.298
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.298)
* b5b1913 - footer override markdown inline parsing (commit by @NGPixel)
* 8db4be6 - expose skipUserProfile option in passport-oidc (PR #6190 by @robinho81)
* 5acc7e7 - add new props to existing auth strategies (PR #6250 by @icsinfo)
* 4de461f - make list paragraphs use 100% width (PR #6273 by @pgi-jsanchez)

[1.10.16]
* Update Wiki.js to 2.5.299
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.299)
* fd00272 - auth: allow custom GitLab endpoints for self-managed instances (PR #6399 by @DerekJarvis)
* e1d282a - warn and exit on unsupported node version (commit by @NGPixel)
* 4e5e830 - add v2 of analytics module umami (PR #6442 by @CDN18)

[1.10.17]
* Do not overwrite the LDAP auth login provider name, as it can be configured in the admin UI

[1.10.18]
* Update Wiki.js to 2.5.300
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.300)
* db8a09f - add ACR Value option to OIDC Module (PR #6553 by @dmc6297)
* d75fc76 - add markdown-it-pivot-table rendering module (PR #6574 by @jaeseopark)

[1.11.0]
* Move to OIDC login

[1.12.0]
* Update base image to 4.2.0

[1.12.1]
* Update Wiki.js to 2.5.301
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.301)
* 38a46e6 - sync groups with SAML provider (PR #6299 by @aelgasser)
* 99e74e8 - upgrade markdown-it-emoji to 3.0.0 (PR #6945 by @j-tai)
* 8932d15 - typo in kroki name (PR #6745 by @p4block)
* b1e1759 - set securityTrustProxy to false by default (commit by @NGPixel)
* c4c41be - upgrade markdown-it-pivot-table version (PR #6707 by @jaeseopark)

[1.12.2]
* Update Wiki.js to 2.5.303
* [Full changelog](https://github.com/requarks/wiki/releases/tag/v2.5.303)
* move template expressions after dom-purify + handle text nodes without parent (commit by @NGPixel)

[1.12.3]
* Update wiki to 2.5.305
* [Full Changelog](https://github.com/requarks/wiki/releases/tag/v2.5.305)
* [`7757de3`](https://github.com/requarks/wiki/commit/7757de3af793fb506ca5435c3c31524b3966c219) - add elasticsearch 8.x support *(MR [#&#8203;6904](https://github.com/Requarks/wiki/issues/6904) by [@&#8203;jacobacon](https://github.com/jacobacon))*
* [`ab5c620`](https://github.com/requarks/wiki/commit/ab5c620d5046cfa4713fca9a44a78477baac4b40) - update yarn lock *(commit by [@&#8203;NGPixel](https://github.com/NGPixel))*
* [`904260f`](https://github.com/requarks/wiki/commit/904260fd44729ed2f75267daebd70499305121f8) - set no-store cache control on jwt renew response *(commit by [@&#8203;NGPixel](https://github.com/NGPixel))*
* [`b9fb17d`](https://github.com/requarks/wiki/commit/b9fb17d4d4a0956ec35e8c73cc85192552fb8d16) - prevent password reset on disabled account *(commit by [@&#8203;NGPixel](https://github.com/NGPixel))*
* [`d1b4c8c`](https://github.com/requarks/wiki/commit/d1b4c8c407961f328fb74492ec03b69c912daa9b) - **helm**: add pod annotations *(MR [#&#8203;7222](https://github.com/Requarks/wiki/issues/7222) by [@&#8203;Mawarii](https://github.com/Mawarii))*

[1.12.4]
* checklist added to CloudronManifest
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.12.5]
* Update wiki to 2.5.306
* [Full Changelog](https://github.com/requarks/wiki/releases/tag/v2.5.306)
* [`403e98d`](https://github.com/requarks/wiki/commit/403e98dced86103cce59583a357865d1ea59ea70) - add git always namespace option *(commit by [@&#8203;NGPixel](https://github.com/NGPixel))*
* [`abc8dce`](https://github.com/requarks/wiki/commit/abc8dce9ccb9b9f90e5681e01abce3929183f6a4) - Allow HA_ACTIVE: True to enable HA mode *(MR [#&#8203;7493](https://github.com/Requarks/wiki/issues/7493) by [@&#8203;Vasfed](https://github.com/Vasfed))*
* [`94d253b`](https://github.com/requarks/wiki/commit/94d253bf596a976a85034a1317b896af5b720b6b) - **helm**: add support for securityContext and resources in sideload container *(MR [#&#8203;7350](https://github.com/Requarks/wiki/issues/7350) by [@&#8203;maarous](https://github.com/maarous))*

