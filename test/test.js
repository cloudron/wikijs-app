#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const PAGE_ID = Math.floor((Math.random() * 100) + 1);
    const PAGE_NAME = 'page' + PAGE_ID;
    const PAGE_TITLE = 'Cloudron page ' + PAGE_ID;

    let app;
    let browser;

    before(function () {
        // needs 1300px width to have full text buttons displayed
        const chromeOptions = new Options().windowSize({ width: 1300, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function waitForApp(done) {
        console.log('Waiting for 10 seconds for app'); // since start.sh starts it temporarily
        setTimeout(done, 10000);
    }

    async function loginOIDC(identifier, password, alreadyAuthenticated = false) {
        await browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}/login`);
        await sleep(5000);

        await waitForElement(By.xpath('//div[@class="login-list"]/div/div/div[2]'));
        await browser.findElement(By.xpath('//div[@class="login-list"]/div/div/div[2]')).click();
        await sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(identifier);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//p[text()="Your content here"]'));
    }


    async function loginAdmin() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.xpath('//div[@class="login-form"]/div[1]//input'));
        await browser.findElement(By.xpath('//div[@class="login-form"]/div[1]//input')).sendKeys('admin@cloudron.local');
        await browser.findElement(By.xpath('//div[@class="login-form"]/div[2]//input')).sendKeys('changeme123');
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//div[@class="login-form"]/button')).click();
        await waitForElement(By.xpath('//div[text()="Welcome to your wiki!" or text()="Untitled Page" or text()="Home page"]'));
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/logout');
        await sleep(2000);
        await waitForElement(By.xpath('//a[@href="/login"]'));
    }

    async function pageExists() {
        await browser.get('https://' + app.fqdn + '/en/' + PAGE_NAME);
        await waitForElement(By.xpath('//div[text()="' + PAGE_TITLE + '"]'));
        await waitForElement(By.xpath('//p[text()="Your content here"]'));
    }

    async function createHomePage() {
        await createPage('home', 'Home page');
    }

    async function createNewPage() {
        await createPage(PAGE_NAME, PAGE_TITLE);
    }

    async function createPage(page_name, page_title) {
        await browser.get('https://' + app.fqdn + '/e/en/' + page_name);

        await waitForElement(By.xpath('//div[text()="Markdown"]'));
        await browser.findElement(By.xpath('//div[text()="Markdown"]')).click();

        await sleep(2000);
        await waitForElement(By.xpath('//label[text()="Title"]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[text()="Title"]/following-sibling::input')).sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(By.xpath('//label[text()="Title"]/following-sibling::input')).sendKeys(page_title);
        await sleep(2000);
        await waitForElement(By.xpath('//span[text()="OK"]'));
        await sleep(2000);
        await browser.findElement(By.xpath('//span[text()="OK"]')).click();
        await sleep(2000);
        await waitForElement(By.xpath('//span[text()="Create"]'));
        await browser.findElement(By.xpath('//span[text()="Create"]')).click();

        await sleep(10000); // time to "render" the page
        await waitForElement(By.xpath('//p[text()="Your content here"]'));
        await sleep(4000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('can create home page', createHomePage);
    it('can create page', createNewPage);
    it('page exists', pageExists);
    it('can logout', logout);

    it('uninstall app (no sso)', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('wait for app', waitForApp);
    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('can create home page', createHomePage);
    it('can create page', createNewPage);
    it('page exists', pageExists);
    it('can logout', logout);

    it('can login with oidc user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));

    it('page exists', pageExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('wait for app', waitForApp);

    it('can login with admin', loginAdmin);
    it('page exists', pageExists);
    it('can logout', logout);

    it('can login with oidc user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));

    it('page exists', pageExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('wait for app', waitForApp);

    it('can login with admin', loginAdmin);
    it('page exists', pageExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('wait for app', waitForApp);

    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('page exists', pageExists);
    it('can logout', logout);

    it('can login with oidc user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));

    it('page exists', pageExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app', function () { execSync(`cloudron install --appstore-id org.wikijs.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('wait for app', waitForApp);

    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('can create home page', createHomePage);
    it('can create page', createNewPage);
    it('page exists', pageExists);
    it('can logout', logout);

    it('can login with oidc user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));

    it('page exists', pageExists);
    it('can logout', logout);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        // need to restart to make changes in place
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('wait for app', waitForApp);

    it('can login with admin', loginAdmin);
    it('page exists', pageExists);
    it('can logout', logout);

    it('can login with oidc user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));
    it('page exists', pageExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
