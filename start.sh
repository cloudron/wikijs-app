#!/bin/bash

set -eu

update_db_config() {
    echo "=> Updating db configuration"

    if [[ -n "${CLOUDRON_OIDC_AUTH_ENDPOINT:-}" ]]; then

        echo "=> Setting up OIDC integration"

        readonly oidcConfig="{\"clientId\":\"${CLOUDRON_OIDC_CLIENT_ID}\",\"clientSecret\":\"${CLOUDRON_OIDC_CLIENT_SECRET}\",\"authorizationURL\":\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\",\"tokenURL\":\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\",\"userInfoURL\":\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\",\"userIdClaim\":\"sub\",\"displayNameClaim\":\"name\",\"emailClaim\":\"email\",\"mapGroups\":false,\"groupsClaim\":\"groups\",\"logoutURL\":\"\",\"scope\":\"openid profile email\",\"useQueryStringForAccessToken\":false,\"enableCSRFProtection\":true}"

        provider_name="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
        PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} \
            -c "INSERT INTO authentication (\"key\", \"order\", \"displayName\", \"strategyKey\", \"isEnabled\", \"config\", \"selfRegistration\", \"domainWhitelist\", \"autoEnrollGroups\") VALUES ('cloudron', 1, '${provider_name//\'/\'\'}', 'oauth2', 't', '{}', 't', '{\"v\":[]}', '{\"v\":[2]}') ON CONFLICT DO NOTHING;"

        PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} \
            -c "UPDATE authentication SET \"order\"=1, \"strategyKey\"='oauth2', \"isEnabled\"='t', \"selfRegistration\"='t', \"config\"='${oidcConfig}' WHERE key='cloudron'"
    fi

    # use $cldrn$ (postgresql dollar quotes string) to escape any single quotes in the mail display name
    mail_value="{\"senderName\":\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Wiki.js}\",\"senderEmail\":\"${CLOUDRON_MAIL_FROM}\",\"host\":\"${CLOUDRON_MAIL_SMTP_SERVER}\",\"port\":${CLOUDRON_MAIL_SMTP_PORT},\"secure\":false,\"user\":\"${CLOUDRON_MAIL_SMTP_USERNAME}\",\"pass\":\"${CLOUDRON_MAIL_SMTP_PASSWORD}\",\"useDKIM\":false,\"dkimDomainName\":\"\",\"dkimKeySelector\":\"\",\"dkimPrivateKey\":\"\"}"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value=\$cldrn\$${mail_value}\$cldrn\$ WHERE key='mail';"

    host_value="{\"v\":\"${CLOUDRON_APP_ORIGIN}\"}"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE settings SET value='${host_value}' WHERE key='host';"
}

wait_for_wikijs() {
    # Wait for app to come up
    while ! curl --fail http://localhost:3000; do
        echo "=> Waiting for app to come up"
        sleep 1
    done
}

setup_admin() {
    echo "=> Setting up admin"
    curl --fail -X POST 'http://localhost:3000/finalize' -H 'Content-Type: application/json' --data "{\"adminEmail\":\"admin@cloudron.local\",\"adminPassword\":\"changeme123\",\"adminPasswordConfirm\":\"changeme123\",\"siteUrl\":\"${CLOUDRON_APP_ORIGIN}\",\"telemetry\":false}"
}

mkdir -p /app/data/data /app/data/ssh

echo "=> Ensuring default configuration"
[[ ! -f /app/data/config.yml ]] && cp /app/pkg/config.yml.template /app/data/config.yml
yq eval -i ".dataPath=\"/app/data/data\"" /app/data/config.yml
yq eval -i ".port=3000" /app/data/config.yml
yq eval -i ".db.type=\"postgres\"" /app/data/config.yml
yq eval -i ".db.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" /app/data/config.yml
yq eval -i ".db.port=\"${CLOUDRON_POSTGRESQL_PORT}\"" /app/data/config.yml
yq eval -i ".db.user=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" /app/data/config.yml
yq eval -i ".db.pass=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" /app/data/config.yml
yq eval -i ".db.db=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" /app/data/config.yml
yq eval -i ".db.ssl=false" /app/data/config.yml
yq eval -i ".ssl.enabled=false" /app/data/config.yml
yq eval -i ".bindIP=\"0.0.0.0\"" /app/data/config.yml
yq eval -i ".offline=false" /app/data/config.yml

echo "=> Ensuring permissions"
chown -R cloudron:cloudron /app/data/

/usr/local/bin/gosu cloudron:cloudron node /app/code/server &
wikijs_pid=$!
wait_for_wikijs

# We test if users table even exitst (first run) or no user in db yet (setup was interrupted)
readonly userCount=`PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -A -t -c "SELECT COUNT(*) FROM users;"`
readonly usersTable=`PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -A -t -c "SELECT to_regclass('users');"`

if [[ "${usersTable}" != "users" || "${userCount}" == "0" ]]; then
    echo "=> Initial admin setup"
    setup_admin
    sleep 5
fi
update_db_config # note: wikijs has to be restarted for db changes to take effect since it seems to cache things
echo "=> Killing previous instance of Wiki.js to reload db changes"
kill -SIGTERM $wikijs_pid

echo "==> Starting wikijs"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/server

